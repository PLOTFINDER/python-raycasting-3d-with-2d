import time

from pygame.locals import *

from ray import *
from renderer import *

# CONST VARIABLES
WINDOW_SIZE = (1200, 600)
BOARD_ZONE_SIZE = (600, 600)
BOARD_SIZE = Vector(10, 10)
CELL_SIZE = 60
ANGLE_PRECISION = 0.5
RAY_PRECISION = 2
ROTATION_STEP = 2

# VARIABLES
mouse_pos = None
fov = 30
direction = 0

# WINDOW INIT
pygame.init()

window = pygame.display.set_mode(WINDOW_SIZE)
pygame.display.set_caption("Ray Casting")
mainClock = pygame.time.Clock()

simulation = True

# SETTINGS
framerate = 60
time_per_frame = 1 / 60
last_time = time.time()

frameCount = 0

# BOARD
board = Board(BOARD_SIZE, CELL_SIZE)

while simulation:
    delta_time = time.time() - last_time
    while delta_time < time_per_frame:
        delta_time = time.time() - last_time

        for event in pygame.event.get():

            if event.type == QUIT:
                simulation = False

    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        direction -= ROTATION_STEP
    if keys[pygame.K_RIGHT]:
        direction += ROTATION_STEP

    mouse_pos = Vector(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])

    rays = calculateNRays(BOARD_ZONE_SIZE, board, mouse_pos, direction, fov, ANGLE_PRECISION, RAY_PRECISION)

    renderBoard(window, board)
    renderRays(window, rays)
    renderWalls(window, rays, fov)
    pygame.display.update()

    last_time = time.time()
    frameCount += 1
