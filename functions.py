from vector import Vector
import math


def angleToVector(p_angle):
    radians = p_angle * (math.pi / 180)
    return Vector(math.cos(radians), math.sin(radians))


def degreesToRadians(p_angle: float):
    return p_angle * (math.pi / 180)


def inBounds(p_pos: Vector, maxBounds: tuple, minBounds=Vector(0, 0)):
    x_inBounds = minBounds.x <= p_pos.x <= maxBounds[0]
    y_inBounds = minBounds.y <= p_pos.y <= maxBounds[1]

    return x_inBounds and y_inBounds


def deltaDistance(p_startPos, p_endPos):
    return math.sqrt((p_startPos[0] - p_endPos[0]) ** 2 + (p_startPos[1] - p_endPos[1]) ** 2)


def distToGrayscale(p_distance):
    return 255 * (1 - p_distance / 1000)


def distToHeight(p_distance):
    return 600 * (1 - p_distance / 860)


def mapValue(value, minValue, maxValue, minBound, maxBound):
    newValue = normalizeValue(value, minValue, maxValue)
    denum = maxBound - minBound
    r = -minBound / denum
    s = 1 / denum
    result = (newValue - r) / s
    return result


def normalizeValue(value, minValue, maxValue):
    return (value - minValue) / (maxValue - minValue)
