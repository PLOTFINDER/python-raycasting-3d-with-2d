import pygame
from functions import *


def renderBoard(window, board):
    window.fill((0, 0, 0))
    cells = board.getCells()
    cellSize = board.getCellSize()
    for y in range(len(cells)):
        for x in range(len(cells[0])):

            positionX = x * cellSize
            positionY = y * cellSize

            if cells[y][x] % 2 == 1:
                pygame.draw.rect(window, "white", (positionX, positionY, cellSize, cellSize))


def renderRays(window, rays):
    for ray in rays:
        startPos = (ray[0].x, ray[0].y)
        endPos = (ray[1].x, ray[1].y)
        pygame.draw.line(window, "grey", startPos, endPos)


def renderWalls(window, rays, fov):
    deltaFOV = len(rays)
    areaPerRay = 600 / deltaFOV

    minFov = 0 - fov/2
    maxFov = 0 + fov / 2
    rayList = []

    for ray in rays:
        rayList.append(ray[2])

    for i in range(len(rayList)):
        rays[i][2] = mapValue(rayList[i], min(rayList), max(rayList), minFov, maxFov)


    for i, ray in enumerate(rays):
        startPos = (ray[0].x, ray[0].y)
        endPos = (ray[1].x, ray[1].y)
        if ray[2] < 0:
            angle = 360 + ray[2]
        elif ray[2] > 360:
            angle = ray[2] - 360
        else:
            angle = ray[2]

        distortedDistance = deltaDistance(startPos, endPos)

        correctDistance = distortedDistance * math.cos(degreesToRadians(angle))

        grayscale = distToGrayscale(distortedDistance)
        color = pygame.Color((int(grayscale), int(grayscale), int(grayscale)))
        height = distToHeight(correctDistance)

        pygame.draw.rect(window, color, (int(600 + i*areaPerRay), int((600 - height)/2), int(areaPerRay), int(height)))
